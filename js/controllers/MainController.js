    myAppModule.controller('MyController', function($scope) {
      $scope.remove = function (scope) {
        scope.remove();
      };

      $scope.toggle = function (scope) {
        scope.toggle();
      };

      $scope.newSubItem = function (scope) {
        var nodeData = scope.$modelValue;
        nodeData.nodes.push({
          id: nodeData.id * 10 + nodeData.nodes.length,
          title: nodeData.title + '.' + (nodeData.nodes.length + 1),
          nodes: []
        });
      };

      $scope.visible = function (item) {
        return !($scope.query && $scope.query.length > 0
        && item.title.indexOf($scope.query) == -1);

      };

      $scope.findNodes = function () {

      };

      $scope.tree2 = [{
        'id': 1,
        'title': 'Nome da Empresa',
        'nodes': []
      },
      {
        'id': 2,
        'title': 'Empresa 2',
        'nodes': []
      }];
    });